import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_ble_nus_template/nus_service.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';

import 'bluetooth_off_screen.dart';

void main() {
  runApp(const NusTemplateApp());
}

class NusTemplateApp extends StatefulWidget {
  const NusTemplateApp({super.key});

  @override
  State<NusTemplateApp> createState() => _NusTemplateAppState();
}

class _NusTemplateAppState extends State<NusTemplateApp> {
  BluetoothAdapterState _adapterState = BluetoothAdapterState.unknown;

  late StreamSubscription<BluetoothAdapterState> _adapterStateStateSubscription;

  @override
  void initState() {
    super.initState();
    _adapterStateStateSubscription =
        FlutterBluePlus.adapterState.listen((state) {
      _adapterState = state;
      setState(() {});
    });
  }

  @override
  void dispose() {
    _adapterStateStateSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget screen = _adapterState == BluetoothAdapterState.on
        ? const ScanScreen()
        : BluetoothOffScreen(adapterState: _adapterState);

    return MaterialApp(
      title: 'NusTemplate App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: screen,
    );
  }
}

class ScanScreen extends StatefulWidget {
  const ScanScreen({super.key});

  @override
  State<ScanScreen> createState() => _ScanScreenState();
}

enum DiscoveryStatus { scanning, connecting }

class _ScanScreenState extends State<ScanScreen> {
  late NusService nusService;

  DiscoveryStatus status = DiscoveryStatus.scanning;

  @override
  void initState() {
    super.initState();
    nusService = NusService();

    nusService.connect();
  }

  @override
  void dispose() {
    nusService.disconnect();
    nusService.dispose();

    super.dispose();
  }

  Future<void> onStopPressed() async {
    try {
      FlutterBluePlus.stopScan();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BLE-NUS'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircularProgressIndicator(),
          Text(status == DiscoveryStatus.scanning
              ? 'Scanning...'
              : 'Connecting...'),
        ],
      )),
    );
  }
}
