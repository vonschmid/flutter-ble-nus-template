import 'dart:async';

import 'package:flutter_blue_plus/flutter_blue_plus.dart';

import 'utils/list_utils.dart';

class NusService {
  static const _nusServiceUuid = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
  static const _rxCharacUuid = '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
  static const _txCharacUuid = '6e400003-b5a3-f393-e0a9-e50e24dcca9e';

  NusService() {
    _init();
  }

  late StreamSubscription<List<ScanResult>> _scanResultsSubscription;
  late StreamSubscription<BluetoothConnectionState>
      _connectionStateSubscription;

  BluetoothDevice? device;

  void _init() {
    _scanResultsSubscription = FlutterBluePlus.scanResults.listen(_onScanResults);
  }

  void _onScanResults(List<ScanResult> results) {
    for (ScanResult result in results) {
      if (containsIgnoreCase(result.advertisementData.serviceUuids, _nusServiceUuid)) {
        FlutterBluePlus.stopScan();

        device = result.device;

        _connectionStateSubscription = device!.connectionState.listen(_onConnectionStateChanged);
        result.device.connect();

        // setState(() {
        //   status = DiscoveryStatus.connecting;
        // });
        return;
      }
    }
  }

  void _onConnectionStateChanged(BluetoothConnectionState state) {
    if (state == BluetoothConnectionState.connected) {
      _discoverServices();
    } else {
      // disconnected
    }
  }

  Future<void> _discoverServices() async {
    assert(device != null);

    try {
      final services = await device!.discoverServices();

      for (var service in services) {
        for (var characteristic in service.characteristics) {
          print(characteristic.uuid);
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> connect() async {
    try {
      await FlutterBluePlus.startScan(timeout: const Duration(seconds: 10));
    } catch (e) {
      print(e);
    }
  }

  void disconnect() {
    device?.disconnect();
  }

  void dispose() {
    _scanResultsSubscription.cancel();
    _connectionStateSubscription.cancel();
  }
}
