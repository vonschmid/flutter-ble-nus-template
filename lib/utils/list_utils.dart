bool containsIgnoreCase(List<String> list, String element) {
  for (String e in list) {
    if (e.toLowerCase() == element.toLowerCase()) return true;
  }
  return false;
}